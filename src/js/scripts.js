
/*IMAGE SLIDER*/
$(document).ready(function(){
  var slideNum = $('.carousel-inner .carousel-item').length;
  var randomNum = Math.floor(Math.random() * slideNum) + 1;
  var randomNumIndex = randomNum - 1;
  $('.carousel').carousel( randomNumIndex );
  $('.carousel-item').removeClass('transparent');
});


/*FLICKR STREAM*/
// Get the modal
var modal = document.getElementById('id01');
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}




/*PAGINATION*/
$(document).ready(function() {
  $('.t1').after('<div id="nav" class="text-center"></div>');
  var rowsShown = 1;
  var rowsTotal = $('.t1 .row').length;
  var numPages = rowsTotal / rowsShown;
  for (i = 0; i < numPages; i++) {
    var pageNum = i + 1;
    $('#nav').append('<a href="#" class="btn-outline-info" rel="' + i + '">&emsp;' + pageNum + '&emsp;</a> ');
  }

  $('.t1 .row').hide();
  $('.t1 .row').slice(0, rowsShown).show();
  $('#nav a:first').addClass('active');
  $('#nav a').bind('click', function(e) {
  	e.preventDefault();
    $('#nav a').removeClass('active');
    $(this).addClass('active');
    var currPage = $(this).attr('rel');
    var startItem = currPage * rowsShown;
    var endItem = startItem + rowsShown;

    $('.t1 .row').css('opacity', '0').hide().slice(startItem, endItem).
    css('display', 'flex').animate({
      opacity: 1
    }, 300);
  });
});




/*READ MORE*/
  document.querySelectorAll(".showmore").forEach(function (p) {
    p.querySelector("button").addEventListener("click", function () {
      p.classList.toggle("show");
      this.textContent = p.classList.contains("show") ? "Show Less" : "Read More";
    });
  });



/*SEARCH BAR WITH HIGHLIGHT*/
  jQuery.fn.highlight = function(pat) {

    function innerHighlight(node, pat) {

        var skip = 0;

        if (node.nodeType == 3) {
            var pos = node.data.toUpperCase().indexOf(pat);
            if (pos >= 0) {

                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);

                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        } else if (node.nodeType == 1 && node.childNodes && !/(script|style) /i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }

    return this.each(function() {
        innerHighlight(this, pat.toUpperCase());
    });

};
jQuery.fn.removeHighlight = function() {
    function newNormalize(node) {
        for (var i = 0, children = node.childNodes, nodeCount = children.length; i < nodeCount; i++) {
            var child = children[i];
            if (child.nodeType == 1) {
                newNormalize(child);
                continue;
            }

            if (child.nodeType != 3) { continue; }

            var next = child.nextSibling;

            if (next == null || next.nodeType != 3) { continue; }

            var combined_text = child.nodeValue + next.nodeValue;
            new_node = node.ownerDocument.createTextNode(combined_text);
            node.insertBefore(new_node, child);
            node.removeChild(child);
            node.removeChild(next);
            i--;
            nodeCount--;
        }
    }
    return this.find("span.highlight").each(function() {

        var thisParent = this.parentNode;
        thisParent.replaceChild(this.firstChild, this);
        newNormalize(thisParent);
    }).end();
};

$(function() {
  $('#text-search').bind('keyup change', function(ev) {
      // pull in the new value
      var searchTerm = $(this).val();
      // remove any old highlighted terms
      $('body').removeHighlight();
      // disable highlighting if empty
      if ( searchTerm ) {
           // highlight the new term
           $('body').highlight( searchTerm );
      }
  });
  
});